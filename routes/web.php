<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;

Route::get('test', function(Request $request) {

    session(['metoo' => 'saved session']);

    echo $request->session()->get('metoo');
});

Route::get('/', 'StaticController@index')->name('home');
Route::get('/about', 'StaticController@about');

Route::get('play', 'PlayController@index');
Route::get('stats', 'StatsController@index');
Route::get('stats/global', 'StatsController@globalStats');

Route::get('play/newgame', 'PlayController@startNewGame');
Route::post('play/guess', 'PlayController@handleAnswer');
Route::get('play/answer', 'PlayController@showAnswer');
Route::get('play/gameover/{hash}', ['as' => 'gameover', 'uses' => 'PlayController@gameOver']);

Route::group(['middleware' => 'auth'],
    function () {
        Route::get('dashboard/', 'DashboardController@index');
        Route::get('dashboard/settings', 'DashboardController@getSettings');
        Route::post('dashboard/settings', 'DashboardController@postSettings');
        Route::get('dashboard/statistics', 'DashboardController@statistics');
        Route::resource('games', 'GameController');
    }
);

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');


// Authentication via Facebook routes...
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

