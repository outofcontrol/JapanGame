<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Questions;

class QuestionsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 30; $i++) {

            Questions::create([
                'en' => $faker->text(15),
                'jp' => $faker->text(15),
                'asked' => $faker->numberBetween(1, 15),
                'correct' => $faker->numberBetween(3, 10)
            ]);
            // TestDummy::times(20)->create('App\Post');
        }
    }
}
