<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        if (App::environment() == 'production') {
            dd("Ack. Don't do this, Thanks Phil!");
        }

        $tables = [
            'questions',
            'games'
        ];

        foreach($tables AS $table) {
            DB::table($table)->truncate();
        }

//        $this->call('QuestionsTableSeeder');
//        $this->command->info('Questions table seeded');
        $this->call('QuestionsRealTableSeeder');
        $this->command->info('Questions table seeded with real question');
        $this->call('GamesTableSeeder');
        $this->command->info('Games table seeded');

    }
}
