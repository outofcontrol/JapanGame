<?php

/*
 * This file is part of Laravel Hashids.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Hashids Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'salt' => 'o*sLu+ROBVyyBxhMKSjL8SMUFLZJvtPLeIdS1m3WeUSWt7WB7V5td%ZdzJL',
            'length' => 'HZCFjYqF7~2EIlEaWB6Y%Jq_dgWPdZcyC4PcT~MoQa_VF0AUu7CxGnG3YymN',
            'alphabet' => '+L=jiN=YGqKXcS|JYAXADeEspe%7dZ8MHyOOFlJQnuYy*ahL7_qFD3s%5',
        ],

        'alternative' => [
            'salt' => 'your-salt-string',
            'length' => 'your-length-integer',
            'alphabet' => 'your-alphabet-string',
        ],

    ],

];
