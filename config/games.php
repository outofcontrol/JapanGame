<?php

return [
    'lives' => 3, // How many lives a new game gets
    'scoreIncrement' => 10, // Incremental value of each correct answer
    'timer' => false, // Should there be a time limit on questions? Not implemented yet.
    'positive' => [
        'Great job!',
        'Way to go!',
        'Good job!',
        'Bingo!',
        'Awesome!',
    ],

    'negative' => [
        'So close!',
        'Not quite right!',
        'Better luck next question!',
        'Oops!',
        'Don\'t give up!',
    ]

];