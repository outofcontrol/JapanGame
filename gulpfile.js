var elixir = require('laravel-elixir');

var paths = {
    'bootstrap': './bower_components/bootstrap-sass-official/assets/',
    'fontawesome': './bower_components/font-awesome-sass/assets/',
    'public': 'public/assets/'
}

elixir(function (mix) {
    mix.sass(
        'app.scss',
        paths.public + 'css',
        {
            includePaths: [
                paths.bootstrap + 'stylesheets/',
                paths.fontawesome + 'stylesheets/'
            ]
        }
    );

});

elixir(function(mix) {
    mix.copy(
        paths.fontawesome + 'fonts',
        paths.public + 'fonts'
    );
});