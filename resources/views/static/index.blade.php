@extends('layouts.layout')


@section('content')

    <div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3">
            <img src="/assets/images/Red-Barn-Logo-250.png" alt="Learning Karate Terms Logo">

            <h1>Learn Karate Terms!</h1>

            <p>There are over two hundred <strong>karate terms</strong> here that are used in <strong>Kenkokan&nbsp;Shorinjiryu&nbsp;Karate</strong>. How many karate words do you know? See how far you can go, with only {{Config::get('games.lives')}} chances to make a mistake.</p>
            <p>Manabu Ninja is Japanese for Learning Ninja... or in this case, learning karate terminology. A fun way to help you learn the karate words that you need to know.</p>

            <p><a class="btn btn-lg btn-success" href="{{ url('play') }}" rel="nofollow">
                    Play <span class="fa fa-arrow-right"></span></a>
            </p>
        </div>
    </div>
@endsection