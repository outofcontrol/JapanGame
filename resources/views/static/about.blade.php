@extends('layouts.layout', array('title' => 'About Karate Terminology'))

@section('content')
    <div class="row">
        <h1>What is Learning Karate Terminology</h1>

        <p>With a belt exam coming up soon, and being a programmer, what better way to learn karate terms than writing a Karate Terminology App. Now... there's an app for that!</p>

        <p>This game is based on the <a href="http://greatlanguagegame.com" rel="nofollow external">Great Language Game</a>,
            which is also a fun app. Thank you <a href="https://manoli.ca/">Sensei George Manoli</a> for the many years of support and immense patience.</p>
    </div>
    <div class="row text-center">
        <h3>Tell your friends!</h3>
        <a class="btn btn-lg btn-info" href=""><span class="fa fa-twitter"></span></a>
        <a class="btn btn-lg btn-info" href=""><span class="fa fa-facebook"></span></a>
    </div>

@endsection