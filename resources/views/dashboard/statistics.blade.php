@extends('layouts.layout')

@section('content')
    <h1>Statistics</h1>
    @if ($gamesPlayed == 0)
    <h2>No Games Played Yet</h2>
    <a class="btn btn-success btn-lg" rel="nofollow" href="/play/newgame">Play a game now</a>
    @else
    <h2>{{ $gamesPlayed }} Games Played</h2>
    {{--<div id="graph" style="width:700px;height:300px;"></div>--}}

    <h2>Game History</h2>
    <table class="table">
        @foreach ($gamesList AS $game)
            <tr><td>Score: {!! $game->score !!}</td><td>Date Played: {!! $game->datePlayed !!}</td></tr>
        @endforeach
    </table>
    @endif
@endsection