@extends('layouts.layout')

@section('content')
    <h1>Settings</h1>
    @include('auth/partials/formerrors')
    {!! Form::open(['url'=> '/dashboard/settings', 'class'=>'form-horizontal', 'role'=>'form']) !!}
    {!! Form::hidden('id', $user->id) !!}
    <h3>Edit account information</h3>

    <div class="form-group">
        <label class="col-sm-2 control-label" for="name">Name:</label>

        <div class="col-sm-10">
            {!! Form::text('name', $user->name, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="name">Email:</label>

        <div class="col-sm-10">
            {!! Form::text('email', $user->email, ['class'=>'form-control']) !!}
        </div>
    </div>
    <h3>Change password</h3>

    <div class="form-group">
        <label class="col-sm-2 control-label" for="name">Current password:</label>

        <div class="col-sm-10">
            {!! Form::password('old_password', ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="new_password">New password:</label>

        <div class="col-sm-10">
            {!! Form::password('new_password', ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="confirm_new_password">Confirm new password:</label>

        <div class="col-sm-10">
            {!! Form::password('confirm_new_password', ['class'=>'form-control']) !!}
        </div>
    </div>
    <h3>Close account</h3>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="cancel_account">Cancel my account</label>

        <div class="col-sm-10">
            {!! Form::button('Proceed to cancel my account', ['class'=>'btn btn-danger']) !!}
        </div>
        <div class="help-block col-sm-offset-2 col-sm-10">Note: Canceling your account will delete the account and all associated data permanently.</div>
    </div>
    <button type="submit" class="col-sm-offset-2 btn btn-success">Save changes</button> or {!! link_to('/dashboard','cancel') !!}
    {!! Form::close() !!}

@endsection