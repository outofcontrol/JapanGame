@extends('layouts.layout')

@section('content')

    <h1>Profile</h1>

    @include ('dashboard.partials.messages')

    <table class="table">
        <tr>
            <td rowspan="3" style="width:250px">
            @if ($user->avatar != '')
                <img src="{!! $user->avatar !!}" alt="" style="width:200px; height: 200px;">
                @else
                <img src="{!! $user->gravatar !!}" alt="" style="width:200px; height: 200px;">
                @endif
                </td>
            <td>Name:</td>
            <td>{!! $user->name !!}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{!! $user->email !!}</td>
        </tr>
        <tr>
            <td>Joined on:</td>
            <td>{!! $user->created_at !!}</td>
        </tr>
    </table>
@endsection