@extends('layouts.layout', array('title' => 'And the answer is!'))

@section('content')

    @include('play.partials.livesandscores')

    <div class="text-center">
        <h2>{!! $phrase !!}</h2>
        @if(Session::get('correct'))
            <h3>The answer was <strong>{{ Session::get('answer') }}</strong></h3>
            <a class="btn btn-lg btn-success" href="/play">Next <span class="fa fa-arrow-right"></span></a>
        @else
            <h3>The correct answer was <strong>{{ Session::get('answer') }}</strong></h3>
            @if($complete)
                <a class="btn btn-lg btn-warning" href="/play">Gameover! <span class="fa fa-arrow-right"></span></a>
            @else
                <a class="btn btn-lg btn-danger" href="/play">Next <span class="fa fa-arrow-right"></span></a>
            @endif
        @endif
    </div>
@endsection