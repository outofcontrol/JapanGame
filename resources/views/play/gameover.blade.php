@extends('layouts.layout', array('title' => 'Game Over'))

@section('content')
    <div class="row text-center">

        <h2>Final score: {!! $score !!}</h2>

        <p><a class="btn btn-lg btn-success" href="/play/newgame">Play Again <span> > </span></a></p>

        <div class="col-sm-6 col-sm-offset-3">
            <table class="table">
                <tr>
                    <th class="text-center">Wrong Answers</th>
                    <th class="text-center">Correct Answers</th>
                </tr>
                @foreach( $wrongs AS $wrong)
                    <tr>
                        <td>{!! $wrong['en'] !!}</td>
                        <td>{!! $wrong['jp'] !!}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="row text-center">
        <h3>Share your score!</h3>
        <a class="btn btn-lg btn-info" href="https://twitter.com/intent/tweet?text=Checkout+my+score+on+Manabu.+Learning+Karate+terms+the+fun+way!&url={{ Request::url() }}&hashtags=karate"><span class="fa fa-twitter" rel="external"></span></a>
    </div>
@endsection