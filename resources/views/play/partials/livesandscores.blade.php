<div class="row">
    <div class="col-xs-6 col-sm-4 col-sm-offset-2"><h3>Lives: {!! $lives !!}</h3></div>
    <div class="col-xs-6 col-sm-4 text-right"><h3>Score: {!! $score !!}</h3></div>
</div>
