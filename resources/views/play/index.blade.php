@extends('layouts.layout')

@section('content')


    @include('play.partials.livesandscores')
    <div class="row">
        <div class="col-sm-12 text-center">
            <h3>The term : <strong>{!! $question['question']->en !!}</strong></h3>
            <h3>Translates to:</h3>

            {!! Form::open(['url' => 'play/guess', 'method' => 'post']) !!}
            {!! Form::hidden('gamehash', $gamehash) !!}
            {!! Form::hidden('questionhash', $questionhash) !!}

            <div class="col-sm-offset-4 col-sm-4">
                @foreach($question['answers'] AS $answer)
                    <button class="btn btn-lg btn-default btn-block" name="answer" value="{{$answer->id}}">{!! $answer->jp !!}</button>
                @endforeach
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection