@extends('layouts.layout', array('title' => 'Karate Terminology Game Stats'))


@section('content')

    <h1>Overview Statistics</h1>
    <h3>{!! $gamesPlayed or 'N/A' !!} Games played</h3>
    <div class="row">
        <div class="col-sm-4">
            <h3>Top Scores:</h3>
            <ul class="list-group">
                @foreach ($gamesTopScores AS $score)
                    <li class="list-group-item">{!! $score->score !!}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-4">
            <h3>Top correct words:</h3>
            <ul class="list-group">
                @foreach ($gamesCorrect AS $correct)
                    <li class="list-group-item">{{ $correct->correct }} : {{$correct->en}} <span
                                class="fa fa-arrow-right"></span> {{$correct->jp}}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-4">
            <h3>Top incorrect words:</h3>
            <ul class="list-group">
                @foreach ($gamesIncorrect AS $incorrect)
                    <li class="list-group-item">{{ $incorrect->correct }} : {{$incorrect->en}} <span
                                class="fa fa-arrow-right"></span> {{$incorrect->jp}}</li>
                @endforeach
            </ul>
        </div>
    </div>
    <h3>Top 5 countries: </h3>
    <ul class="list-group">
        @foreach ($gamesTopCountries AS $country)
            <li class="list-group-item">{!! $country->location_country or 'N/A' !!}</li>
        @endforeach
    </ul>
@endsection
