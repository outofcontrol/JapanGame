@if (Auth::Check())
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="{{ url('/dashboard/') }}">Profile</a></li>
            <li><a href="{{ url('/dashboard/settings') }}">Settings</a></li>
            <li><a href="{{ url('/dashboard/statistics') }}">My statistics</a></li>
            <li><a href="{{ url('/logout') }}">Logout</a></li>
        </ul>
    </li>
@else
    <li><a href="{{ url('/login') }}">Login</a></li>
@endif
