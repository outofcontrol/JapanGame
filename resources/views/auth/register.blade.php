@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Register</h3>
                </div>
                <div class="panel-body">
                    <form id="register-form" class="text-left" method="POST" action="/auth/register">
                        {!! csrf_field() !!}
                        <div class="login-form-main-message"></div>
                        <fieldset>
                        <div class="form-group">
                            {!! Form::text('name', old('name'), ['id'=>'reg_username', 'class'=>'form-control', 'placeholder'=>'Your name']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::password('password', ['class'=>'form-control', 'id'=>'reg_password', 'placeholder'=>'Password']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::password('password_confirmation', ['class'=>'form-control', 'id'=>'reg_password_confirm', 'placeholder'=>'Confirm Password']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::email('email', old('email'), ['class'=>'form-control','id'=>'reg_email','placeholder'=>'Your Email Address']) !!}
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Register">
                        </fieldset>
                    </form>
                    <hr>
                    <center><h4>OR</h4></center>
                    <a class="btn btn-lg btn-info btn-block" href="/auth/facebook">Login via facebook</a>
                    <hr>
                    <p>Already have an account? <a href="/auth/login">Login here</a></p>
                </div>
            </div>
        </div>
    </div>

@endsection
