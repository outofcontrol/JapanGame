@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Reset password</h3>
                </div>
                @include('auth.partials.formerrors')
                <div class="panel-body">
                <form id="register-form" class="text-left" method="POST" action="/password/reset">
                    <fieldset>
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        {!! Form::email('email', old('email'), ['class'=>'form-control','id'=>'reg_email','placeholder'=>'Your Email']) !!}
                        <label for="reg_email" class="sr-only">Email</label>
                    </div>
                    <div class="form-group">
                        <label for="reg_password" class="sr-only">Password</label>
                        {!! Form::password('password', ['class'=>'form-control', 'id'=>'reg_password', 'placeholder'=>'Password']) !!}
                    </div>
                    <div class="form-group">
                        <label for="reg_password_confirm" class="sr-only">Password Confirm</label>
                        {!! Form::password('password_confirmation', ['class'=>'form-control', 'id'=>'reg_password_confirm', 'placeholder'=>'Confirm Password']) !!}
                    </div>
                    <input type="submit" class="btn btn-success btn-lg" value="Reset Password">
                </form>
            </div>
        </div>
    </div>

@endsection
