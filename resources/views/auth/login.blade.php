@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login via site</h3>
                </div>
                @include('auth.partials.formerrors')
                <div class="panel-body">
                    <form id="login-form" class="text-left" method="POST" action="/login">
                        {!! csrf_field() !!}
                        <fieldset>
                        <div class="form-group">
                            <label for="lg_username" class="sr-only">Username</label>
                            {!! Form::text('email', old('email'), ['id' => 'lg_username', 'placeholder' => 'Your Email Address', 'class' => 'focus form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="lg_password" class="sr-only">Password</label>
                            {!! Form::password('password', ['class'=>'form-control','id'=>'lg_password','placeholder'=>'Password']) !!}
                        </div>
                        <div class="form-group">
                             {!! Form::checkbox('remember', old('remember'), null, ['id'=>'lg_remember']) !!}
                            <label for="lg_remember">Remember Me</label>
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                    </fieldset>
                    </form>
                    <hr>
                    <center><h4>OR</h4></center>
                    <a class="btn btn-lg btn-info btn-block" href="/login/facebook"><span class="fa fa-facebook">&nbsp;&nbsp;Login via facebook</a>
                    <hr>
                    <p>Forgot your password? <a href="/password/email">Click here</a></p>
                    <p>New user? <a href="/auth/register">Create new account</a></p>

                </div>
            </div>
        </div>
    </div>

@endsection