@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Forgot Password</h3>
                </div>
                <div class="panel-body">
                    <form id="forgot-password-form" class="text-left" action="/password/email" method="POST">
                        {!! csrf_field() !!}
                        <fieldset>
                            <div class="form-group">
                                <p>When you fill in your registered email address, you will be sent instructions on how to reset your password.</p>
                            </div>
                            <div class="login-form-main-message"></div>
                            <div class="form-group">
                                <label for="fp_email" class="sr-only">Email address</label>
                                <input type="text" class="form-control" id="fp_email" name="email" placeholder="Your email address">
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Reset Password">
                        </fieldgroup>
                    </form>
                    <hr>
                    <p>Already have an account? <a href="/auth/login">Login here</a></p>
                    <p>New user? <a href="/auth/register">Create new account</a></p>
                </div>
            </div>
        </div>
    </div>

@endsection
