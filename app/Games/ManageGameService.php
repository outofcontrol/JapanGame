<?php namespace App\Games;

use App\Scores;
use App\Questions;
use Request;
use Session;
use Config;
use Hashids;

/**
 * Class ManageGameService
 * @package App\Games
 */
class ManageGameService
{
    /**
     * Holds the game data
     * @var
     */
    public $game;

    public function __construct(Questions $questions, Scores $scores)
    {
        $this->questions = $questions;
        $this->scores = $scores;
    }

    /**
     * Load a new or previous game
     */
    public function setup()
    {
        $this->game = $this->scores->loadGame($this->getHash());

        // We've got an invalid game hash, bail!
        if ($this->game == null) {
            $this->forgetGame();
            abort(404);
        }
    }

    /**
     * Getter for game
     *
     * @return object
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Return one question with answers
     *
     * @return array
     */
    public function getQuestion()
    {
        $this->game['question'] = $this->questions->getQandA($this->questionsAlreadyAsked());
        $this->game['questionhash'] = Hashids::encode($this->game['question']['question']->id); 

        return $this->getGame();
    }

    /**
     * Handle user response answer
     * @return array
     */
    public function handleAnswer()
    {
        $qid = $this->getQuestionID();
        $correctAnswer = $this->isAnswerCorrect($qid);
        $question = '';

        if ($this->updateGameData($qid, $correctAnswer)) {
            $this->scores->updateGame($this->getGame(), $correctAnswer);
            $question = $this->questions->updateQuestion($qid, $correctAnswer);
        }
        return ['answer' => $question, 'correct' => $correctAnswer];
    }

    public function getAnswer($correct)
    {
        $this->getPhrase($correct);
        return $this->getGame();
    }

    /**
     * Return wrong answers stored in game data
     *
     * @return array
     */
    public function getCompleteGame()
    {
        $this->game['wrongs'] = $this->questions->getWrongQuestions($this->questionsIncorrect());
        $this->forgetGame();

        return $this->getGame();
    }

    /**
     * Drop the session for the current game
     */
    public function forgetGame()
    {
        if ($this->game == null || $this->game['gamehash'] == Session::get('gamehash')) {
            Session::forget('gamehash');
        }
    }

    /**
     * Drop the session for the current game and delete from DB
     */
    public function startNewGame()
    {
        $this->scores->deleteGame(Session::get('gamehash'));
        $this->forgetGame();
    }

    /**
     * Check if current answer is correct
     *
     * @param $qid
     * @return bool
     */
    private function isAnswerCorrect($qid)
    {
        return ($qid == Request::get('answer'));
    }

    /**
     * Decode Question ID hash and return. Return false if non-existent ID
     * @return mixed
     */
    private function getQuestionID()
    {
        $question = Hashids::decode(Request::get('questionhash'));

        return is_integer($question[0]) ? $question[0] : 0;
    }

    /**
     * Return a random congratulatory phrase for correct and incorrect answer.
     * @param bool|true $intent
     */
    private function getPhrase($intent = true)
    {
        $phrases = Config::get('games.' . ($intent ? 'positive' : 'negative'));
        $idx = rand(0, count($phrases) - 1);
        $this->game['phrase'] = $phrases[$idx];
    }

    /**
     * Update game data in DB
     * @param $qid
     * @param $correctAnswer
     */
    private function updateGameData($qid, $correctAnswer)
    {
        $gameData = $this->getGameData();

        // Duplicate question, don't update anything! Probably a cheater
        if ($gameData && in_array($qid, $gameData->asked)) {
            return false;
        }
        $gameData->asked[] = $qid;
        if (!$correctAnswer) {
            $gameData->wrong[] = $qid;
        }
        $this->setGameData($gameData);
        return true;
    }

    /**
     * Return array of incorrect answer IDs from game data
     *
     * @return array
     */
    public function questionsIncorrect()
    {
        $gameData = $this->getGameData();
        return (isset($gameData->wrong)) ? $gameData->wrong : [];
    }

    /**
     * Return array of previously asked question IDs for current game
     * @return array
     */
    public function questionsAlreadyAsked()
    {
        $gameData = $this->getGameData();
        return (isset($gameData->asked)) ? $gameData->asked : [];
    }

    /**
     * Game data setter
     * @param $gameData
     */
    public function setGameData($gameData)
    {
        $this->game->game_data = json_encode($gameData);
    }

    /**
     * Game Data getter
     * @return mixed
     */
    public function getGameData()
    {
        return json_decode($this->game->game_data);
    }

    /**
     * Returns true of game is over, false if game is still going
     * @return bool
     */
    public function gameOver()
    {
        return $this->game['complete'];
    }

    /**
     * Return a game hash
     * @return string
     */
    public function getHash()
    {
        $hash = null;

        if (Request::segment(3)) {
            $hash = Request::segment(3);
        }

        if (!$hash) {
            $hash = Session::get('gamehash');
        }

// dd($hash);
        if (!$hash) {
            $hash = md5(time());
            $this->scores->createNewGame($hash);
        }

        Session::put('gamehash', $hash);

        return $hash;
    }
}
