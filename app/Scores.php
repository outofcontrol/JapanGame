<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Session;
use Hash;
use DB;
use GeoIP;
use Config;
use Auth;

class Scores extends Model
{
    protected $fillable = ['gamehash', 'score', 'lives', 'game_data'];


    /**
     * Return Statistics for top countries
     * @param int $limit
     * @return object
     */
    public function getStatsTopCountry($limit = 5)
    {
        return $this->select('location_country', DB::Raw('count(*) AS total'))->groupBy('location_country')->orderBy('total')->take($limit)->get();
    }


    /**
     * Return List of all games played by user
     * @param null $userId
     * @param int $limit
     * @return object
     */
    public function getStatsUserGamesList($userId = null, $limit = 20)
    {
        return $this->select(['score','created_at AS datePlayed'])->where('user_id', $userId)->where('complete', '1')->orderBy('created_at', 'DESC')->take($limit)->get();
    }


    /**
     * Return count of games played by a specific user
     * @param null $userId
     * @return integer
     */
    public function getStatsGamesPlayed($userId = null)
    {
        $games = $this->query();

        if ($userId) {
            $games->where('user_id', $userId);
        }
        return $games->where('complete', '1')->count();
    }


    /**
     * Return list of top 5 scores of all time
     * @param int $limit
     * @return object
     */
    public function getStatsTopScore($limit = 5)
    {
        return $this->select('score')->orderBy('score', 'DESC')->limit($limit)->get();
    }

    /**
     * Delete a game
     * @param $hash
     */
    public function deleteGame($hash)
    {
        $this->where('gamehash', $hash)->delete();
    }

    /**
     * Load a single game
     * @param $hash
     * @return mixed
     */
    public function loadGame($hash)
    {
        return $this->where('gamehash', $hash)->first();
    }

    /**
     * Create a single game
     * @param $hash
     */
    public function createNewGame($hash)
    {
        $game = $this->firstOrCreate([
            'gamehash' => $hash,
        ]);
        $location = GeoIP::getLocation();
        $game->lives = Config::get('games.lives');
        $game->location_country = $location['iso_code'];
        $game->location_city = $location['city'];
        $game->ip = Request::getClientIp();
        $user = Auth::User();
        $game->user_id = (isset($user->id)) ? $user->id : 0;

        $game->save();
    }

    /**
     * Update a single game with new data from question/answer
     * @param $questions
     * @return bool
     */
    public function updateGame($game, $correctAnswer)
    {
        if ($correctAnswer) {
            $game->increment('score', 10);
        } elseif ($game->lives > 0) {
            $game->decrement('lives');
        }
        $game->complete = ($game->lives < 1) ? true : false;
        $game->save();
    }
}
