<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use Session;
use App\Scores;

class DashboardController extends Controller
{
    /**
     * User Dashboard
     *
     * @return Response
     */
    
    public function __construct(Scores $scores)
    {
        $this->scores = $scores;
    }

    public function index()
    {
        return view('dashboard.index', ['user' =>Auth::user(), 'message' => Session::get('message')]);
    }

    public function getSettings()
    {
        return view('dashboard.settings', ['user' =>Auth::user()]);
    }

    public function postSettings(\App\Http\Requests\UpdateProfileRequest $request)
    {
        $user = Auth::User();
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password) {
            $user->password = $request->new_password;
        }
        $user->save();
        return Redirect::to('dashboard')->with('message', 'Profile successfully update!');
    }

    public function statistics()
    {
        $user = Auth::user();
        $data['gamesPlayed'] = $this->scores->getStatsGamesPlayed($user->id);
        $data['gamesList'] = $this->scores->getStatsUserGamesList($user->id);
        return view('dashboard.statistics', $data);
    }
}
