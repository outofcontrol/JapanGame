<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Scores;
use App\Questions;

class StatsController extends Controller
{

    public function __construct(Questions $questions, Scores $scores)
    {
        $this->questions = $questions;
        $this->scores = $scores;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $statistics = [
            'gamesPlayed' => $this->scores->getStatsGamesPlayed(),
            'gamesTopScores' => $this->scores->getStatsTopScore(),
            'gamesCorrect' => $this->questions->getStatsCorrectWords(5),
            'gamesIncorrect' => $this->questions->getStatsIncorrectWords(5),
            'gamesTopCountries' => $this->scores->getStatsTopCountry(5),
        ];
        return view('stats.index', $statistics);
    }

    public function globalStats()
    {
        return view('stats.global');
    }

}
