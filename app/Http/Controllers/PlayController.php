<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Games\ManageGameService;

class PlayController extends Controller
{
    /**
     * @var Games\ManageGameService
     */
    private $managegame;

    function __construct(ManageGameService $managegame)
    {
        $this->managegame = $managegame;
    }

    /**
     * Start play of the game.
     *
     * @return Response
     */
    public function index()
    {
        $this->managegame->setup();

        if ($this->managegame->gameOver()) {
            return redirect('play/gameover/' . $this->managegame->getHash());
        }
        try {
            $question = $this->managegame->getQuestion();
        } catch (\Exception $e) {
            return redirect('home');
        }
        return view('play.index', $question);
    }

    /**
     * Show results of user guess
     */
    public function handleAnswer()
    {
        $this->managegame->setup();

        $answer = $this->managegame->handleAnswer();

        return redirect('play/answer')
            ->with('correct', $answer['correct'])
            ->with('answer', $answer['answer']);
    }

    public function showAnswer()
    {
        $this->managegame->setup();

        return view('play.answer', $this->managegame->getAnswer(session('correct')));
    }

    public function startNewGame()
    {
        $this->managegame->setup();

        $this->managegame->startNewGame();

        return redirect('play');
    }

    /**
     * Game over!
     */
    public function gameOver()
    {
        $this->managegame->setup();

        return view('play.gameover', $this->managegame->getCompleteGame());
    }
}
