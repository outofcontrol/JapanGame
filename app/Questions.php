<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Questions extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Load a Question and Answer set
     * @return array
     */
    public function getQandA($questionsAsked)
    {
        $question = $this->whereNotIn('id', $questionsAsked)->orderByRaw("RAND()")->first();
        $answers = $this->orderByRaw("RAND()")->where('id', '!=', $question->id)->take(3)->get();
        $answers->push($question);
        return (['question' => $question, 'answers' => $answers->shuffle()]);
    }

    /**
     * Return list of top correct words
     * @param int $limit
     * @return mixed
     */
    public function getStatsCorrectWords($limit = 5)
    {
        return $this->select(['en', 'jp', 'correct'])
                    ->orderBy('correct', 'DESC')
                    ->take($limit)
                    ->get();
    }

    /**
     * Return list of top incorrect words
     * @param int $limit
     * @return mixed
     */
    public function getStatsIncorrectWords($limit = 5)
    {
        return $this->where('asked','>', 0)
                    ->select(['en','jp',DB::Raw('asked-correct AS incorrect'),'asked','correct'])
                    ->orderBy(DB::Raw('asked-correct'), 'ASC')
                    ->take($limit)
                    ->get();
    }

    /**
     * Return list of questions
     * @param $ids
     * @return object
     */
    public function getWrongQuestions($ids)
    {
        return $this->find($ids);
    }

    /**
     * Updates Single Question based on play result
     * @param $questionId
     * @param $correctAnswer
     * @return mixed
     */
    public function updateQuestion($questionId, $correctAnswer)
    {
        $question = $this->find($questionId);
        $question->increment('asked');
        if ($correctAnswer) $question->increment('correct');
        $question->save();

        return $question->jp;
    }
}
